<?php

namespace App\Repositories\Konfigurasi;

use App\Models\Konfigurasi\Menu;

class MenuRepository
{
    public function getMainMenus()
    {
        return Menu::whereNull('parent_id')->select('id', 'name')->get();
    }

    public function getMenus()
    {
        return Menu::with(['subMenus' => function ($query) {
            $query->orderBy('orders');
        }])->whereNull('parent_id')->active()->orderBy('orders')->get();
    }

    public function getMainMenuWithPermission()
    {
        return Menu::with('permissions', 'subMenus.permissions')->whereNull('parent_id')->get();
    }
}
