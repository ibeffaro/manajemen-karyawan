<?php

use Illuminate\Support\Facades\Cache;
use App\Repositories\Konfigurasi\MenuRepository;
use Illuminate\Support\Collection;

if (!function_exists('responseError')) {
    /**
     * Menangani respons error.
     *
     * @param mixed $th
     * @return \Illuminate\Http\Response
     */
    // function responseError(\Exception | string $th)
    function responseError($th): \Illuminate\Http\Response
    {
        $message    = 'Terjadi kesalahan, silahkan coba beberapa saat lagi';
        if ($th instanceof \Exception) {
            if (config('app.debug')) {
                $message    = $th->getMessage();
                $message    .= ' in line ' . $th->getLine() . ' at ' . $th->getFile();
                $data       = $th->getTrace();
            }
        } else {
            $message = $th;
        }
        return response()->json([
            'status'    => 'error',
            'message'   => $message,
            'errors'    => $data ?? null
        ], 500);
    }
}

if (!function_exists('responseSuccess')) {
    function responseSuccess($isEdit = false)
    {
        return response()->json([
            'status'    => 'success',
            'message'   => $isEdit ? 'Data Updated Successfully' : 'Data Saved Successfully'
        ]);
    }
}

if (!function_exists('responseSuccessDelete')) {
    function responseSuccessDelete()
    {
        return response()->json([
            'status'    => 'success',
            'message'   => 'Data Deleted Successfully'
        ]);
    }
}

if (!function_exists('menus')) {
    /**
     * @return Collection
     */
    function menus()
    {
        $menus = (new MenuRepository())->getMenus();
        return $menus;
    }
}

if (!function_exists('urlMenu')) {
    function urlMenu()
    {
        $url    = [];
        foreach (menus() as $mm) {
            $url[] = $mm->url;
            foreach ($mm->subMenus as $sm) {
                $url[] = $sm->url;
            }
        }
        return $url;
    }
}

if (!function_exists('user')) {
    /**
     * @param string $id
     * @return \App\Models\User | string
     */
    function user($id = null)
    {
        if ($id) {
            return request()->user()->{$id};
        }
        return request()->user();
    }
}
