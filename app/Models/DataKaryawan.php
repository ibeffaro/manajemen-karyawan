<?php

namespace App\Models;

use App\Models\MasterData\Jabatan;
use App\Models\MasterData\TempatKerja;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataKaryawan extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    // public function jabatans()
    // {
    //     return $this->hasMany(Jabatan::class, 'jabatan_id');
    // }

    // public function tempat_kerjas()
    // {
    //     return $this->hasMany(TempatKerja::class, 'tempat_kerja_id');
    // }
}
