<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class DataKaryawanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name'      => 'required',
            'phone'     => 'numeric',
            'email'     => ['required', Rule::unique('data_karyawans')->ignore($this->data_karyawan)],
            'password'  => [Rule::requiredIf(function () {
                return request()->routeIs('data-karyawan.store');
            }), 'confirmed'],
        ];
    }
}
