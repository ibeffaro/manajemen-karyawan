<?php

namespace App\Http\Controllers\Konfigurasi;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\Konfigurasi\Menu;
use App\Http\Controllers\Controller;
use App\DataTables\Konfigurasi\RoleDataTable;

class AksesRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(RoleDataTable $roleDataTable)
    {
        return $roleDataTable->render('konfigurasi.akses-role.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
        //
    }

    private function getMenus()
    {
        return Menu::with('permissions', 'subMenus.permissions')->whereNull('parent_id')->get();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $role)
    {
        $getRole = Role::where('id', '!=', $role->id)->get();
        return view('konfigurasi.akses-role.form', [
            'data'      => $role,
            'action'    => route('konfigurasi.akses-role.update', $role->id),
            'menu'      => $this->getMenus(),
            'role'      => $getRole
        ]);
    }

    public function getPermissionByRole(Role $role)
    {
        return view('konfigurasi.akses-role.list-menu', [
            'data'  => $role,
            'menu'  => $this->getMenus()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Role $role)
    {
        $role->syncPermissions($request->permission);

        return responseSuccess(true);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role)
    {
        return response()->json([
            'status'    => 'error',
            'message'   => 'Data Cannot Deleted!'
        ]);
    }
}
