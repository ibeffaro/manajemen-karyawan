<?php

namespace App\Http\Controllers\Konfigurasi;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Konfigurasi\UserDataTable;
use App\Http\Requests\Konfigurasi\UserListRequest;

class UserListController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('konfigurasi.user-list.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('konfigurasi.user-list.form', [
            'data'      => new User(),
            'action'    => route('konfigurasi.user-list.store'),
            'role'      => Role::get()->pluck('name')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserListRequest $request, User $user)
    {
        $user->fill($request->only(['name', 'email']));
        $user->password = bcrypt($request->password);
        $user->markEmailAsVerified();

        $user->save();
        $user->assignRole($request->role);

        return responseSuccess();
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        return view('konfigurasi.user-list.form', [
            'data'      => $user,
            'action'    => route('konfigurasi.user-list.update', $user->id),
            'role'      => Role::get()->pluck('name')
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserListRequest $request, User $user)
    {
        $user->fill($request->only(['name', 'email']));
        $user->password = bcrypt($request->password);

        $user->save();
        $user->syncRoles($request->role);

        return responseSuccess(true);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $response = response()->json([
            'status'    => 'error',
            'message'   => 'Data Cannot Deleted!'
        ]);
        if ($user->id <> 1) {
            $user->delete();
            $response = responseSuccessDelete();
        }
        return $response;
    }
}
