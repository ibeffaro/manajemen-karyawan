<?php

namespace App\Http\Controllers\Konfigurasi;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Konfigurasi\RoleDataTable;
use App\Http\Requests\Konfigurasi\RoleRequest;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(RoleDataTable $roleDataTable)
    {
        return $roleDataTable->render('konfigurasi.role.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('konfigurasi.role.form', [
            'data'      => new Role,
            'action'    => route('konfigurasi.roles.store')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RoleRequest $request)
    {
        $role = new Role($request->validated());
        $role->save();

        return responseSuccess();
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $role)
    {
        $action = route('konfigurasi.roles.update', $role->id);
        return view('konfigurasi.role.form', [
            'data'      => $role,
            'action'    => $action
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RoleRequest $request, Role $role)
    {
        $role->fill($request->validated());
        $role->save();

        return responseSuccess(true); // true:is_edit
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return responseSuccessDelete();
    }
}
