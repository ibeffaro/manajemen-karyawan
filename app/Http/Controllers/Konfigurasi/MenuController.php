<?php

namespace App\Http\Controllers\Konfigurasi;

use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\Konfigurasi\Menu;
use App\Traits\HasMenuPermission;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\DataTables\Konfigurasi\MenuDataTable;
use App\Http\Requests\Konfigurasi\MenuRequest;
use Illuminate\Support\Testing\Fakes\BatchFake;
use App\Repositories\Konfigurasi\MenuRepository;

class MenuController extends Controller
{
    use HasMenuPermission;

    private MenuRepository $repository;
    // public function __construct(private MenuRepository $repository)
    public function __construct(MenuRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(MenuDataTable $menuDataTable)
    {
        return $menuDataTable->render('konfigurasi.menu.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Menu $menu)
    {
        // $this->authorize('create konfigurasi/menu');
        return view('konfigurasi.menu.form', [
            'action'    => route('konfigurasi.menu.store'),
            'data'      => $menu,
            'main_menu' => $this->repository->getMainMenus()
        ]);
    }

    private function fillData(MenuRequest $request, Menu $menu)
    {
        $menu->fill($request->validated());
        $menu->fill([
            'icon'      => $request->icon,
            'parent_id' => $request->parent_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MenuRequest $request, Menu $menu)
    {
        DB::beginTransaction();
        try {
            // $this->authorize('create konfigurasi/menu');
            $this->fillData($request, $menu);
            $menu->save();

            foreach ($request->permissions ?? [] as $val) {
                $permission = Permission::create(['name' => $val . " {$menu->url}"]);
                $permission->menus()->attach($menu);
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();

            return responseError($th);
        }

        return responseSuccess();
    }

    /**
     * Display the specified resource.
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Menu $menu)
    {
        // $this->authorize('update konfigurasi/menu');
        return view('konfigurasi.menu.form', [
            'action'    => route('konfigurasi.menu.update', $menu->id),
            'data'      => $menu,
            'main_menu' => $this->repository->getMainMenus()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MenuRequest $request, Menu $menu)
    {
        // $this->authorize('update konfigurasi/menu');
        $this->fillData($request, $menu);
        if ($request->level_menu == 'main_menu') {
            $menu->parent_id = null;
        }
        $menu->save();

        return responseSuccess(true);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Menu $menu)
    {
        //
    }
}
