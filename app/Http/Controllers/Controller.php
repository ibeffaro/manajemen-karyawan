<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Traits\HasPermission;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests, HasPermission;
}
