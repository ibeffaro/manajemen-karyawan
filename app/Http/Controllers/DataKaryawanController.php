<?php

namespace App\Http\Controllers;

use App\Models\DataKaryawan;
use Illuminate\Http\Request;
use App\DataTables\DataKaryawanDataTable;
use App\Http\Requests\DataKaryawanRequest;

class DataKaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(DataKaryawanDataTable $dataKaryawanDataTable)
    {
        return $dataKaryawanDataTable->render('data-karyawan.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('data-karyawan.form', [
            'data'      => new DataKaryawan,
            'action'    => route('data-karyawan.store')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(DataKaryawanRequest $request)
    {
        $role = new DataKaryawan($request->validated());
        $role->save();

        return responseSuccess();
    }

    /**
     * Display the specified resource.
     */
    public function show(DataKaryawan $dataKaryawan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DataKaryawan $dataKaryawan)
    {
        $action = route('data-karyawan.update', $dataKaryawan->id);
        return view('data-karyawan.form', [
            'data'      => $dataKaryawan,
            'action'    => $action
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(DataKaryawanRequest $request, DataKaryawan $dataKaryawan)
    {
        $dataKaryawan->fill($request->validated());
        $dataKaryawan->save();

        return responseSuccess(true);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DataKaryawan $dataKaryawan)
    {
        $dataKaryawan->delete();
        return responseSuccessDelete();
    }
}
