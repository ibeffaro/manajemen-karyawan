<?php

namespace App\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use App\Models\MasterData\Jabatan;
use App\Http\Controllers\Controller;
use App\DataTables\MasterData\JabatanDataTable;
use App\Http\Requests\MasterData\JabatanRequest;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(JabatanDataTable $jabatanDataTable)
    {
        return $jabatanDataTable->render('master-data.jabatan.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('master-data.jabatan.form', [
            'data'      => new Jabatan,
            'action'    => route('master-data.jabatan.store')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(JabatanRequest $request)
    {
        $jabatan = new Jabatan($request->validated());
        $jabatan->save();

        return responseSuccess();
    }

    /**
     * Display the specified resource.
     */
    public function show(Jabatan $jabatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Jabatan $jabatan)
    {
        $action = route('master-data.jabatan.update', $jabatan->id);
        return view('master-data.jabatan.form', [
            'data'      => $jabatan,
            'action'    => $action
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(JabatanRequest $request, Jabatan $jabatan)
    {
        $jabatan->fill($request->validated());
        $jabatan->save();

        return responseSuccess(true);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Jabatan $jabatan)
    {
        $jabatan->delete();
        return responseSuccessDelete();
    }
}
