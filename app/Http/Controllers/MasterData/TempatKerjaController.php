<?php

namespace App\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use App\Models\MasterData\TempatKerja;
use App\Http\Controllers\Controller;
use App\DataTables\MasterData\TempatKerjaDataTable;
use App\Http\Requests\MasterData\TempatKerjaRequest;

class TempatKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(TempatKerjaDataTable $tempatKerjaDataTable)
    {
        return $tempatKerjaDataTable->render('master-data.tempat-kerja.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('master-data.tempat-kerja.form', [
            'data'      => new TempatKerja,
            'action'    => route('master-data.tempat-kerja.store')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TempatKerjaRequest $request)
    {
        $tempatKerja = new TempatKerja($request->validated());
        $tempatKerja->save();

        return responseSuccess();
    }

    /**
     * Display the specified resource.
     */
    public function show(TempatKerja $tempatKerja)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TempatKerja $tempatKerja)
    {
        $action = route('master-data.tempat-kerja.update', $tempatKerja->id);
        return view('master-data.tempat-kerja.form', [
            'data'      => $tempatKerja,
            'action'    => $action
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TempatKerjaRequest $request, TempatKerja $tempatKerja)
    {
        $tempatKerja->fill($request->validated());
        $tempatKerja->save();

        return responseSuccess(true);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TempatKerja $tempatKerja)
    {
        $tempatKerja->delete();
        return responseSuccessDelete();
    }
}
