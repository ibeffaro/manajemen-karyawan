<?php

namespace App\Traits;

trait DatatableHelper
{
    public function basicActions($row): array
    {
        $action             = [];
        $action['detail']   = route(str_replace('/', '.', request()->path()) . '.show', $row->id);
        if (user()->can('update ' . request()->path())) {
            $action['edit'] = route(str_replace('/', '.', request()->path()) . '.edit', $row->id);
        }
        if (user()->can('delete ' . request()->path())) {
            $action['delete']   = route(str_replace('/', '.', request()->path()) . '.destroy', $row->id);
            $action['id']       = $row->id;
        };

        return $action;
    }
}
