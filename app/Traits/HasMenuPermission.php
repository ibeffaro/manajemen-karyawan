<?php

namespace App\Traits;

use App\Models\Konfigurasi\Menu;
use App\Models\Permission;

trait HasMenuPermission
{
    // public function attachMenuPermission(Menu $menu, array | null $permissions, array | null $roles)
    public function attachMenuPermission(Menu $menu, ?array $permissions = null, ?array $roles = null)
    {
        /**
         * @var Permission $permission
         */

        if (!is_array($permissions)) {
            $permissions = ['create', 'read', 'update', 'delete'];
        }

        foreach ($permissions as $val) {
            $permission = Permission::create(['name' => $val . " {$menu->url}"]);
            $permission->menus()->attach($menu);

            if ($roles) {
                $permission->assignRole($roles);
            }
        }
    }
}
