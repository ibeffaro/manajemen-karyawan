<?php

namespace App\Traits;

use Illuminate\Support\Arr;

trait HasPermission
{
    protected $abilites = [
        // private $abilites = [
        'create'    => 'create',
        'store'     => 'create',
        'show'      => 'read',
        'index'     => 'read',
        'edit'      => 'update',
        'update'    => 'update',
        'destroy'   => 'delete'
    ];

    public function callAction($method, $parameters)
    {
        $action = Arr::get($this->abilites, $method);

        if (!$action) {
            return parent::callAction($method, $parameters);
        }

        $_staticPath    = request()->route()->getCompiled()->getStaticPrefix();
        $staticPath     = substr($_staticPath, 1);
        $urlMenu        = urlMenu();

        if (!in_array($staticPath, $urlMenu)) {
            foreach (array_reverse(explode('/', $staticPath)) as $path) {
                $staticPath = str_replace("/$path", "", $staticPath);
                if (in_array($staticPath, $urlMenu)) {
                    break;
                }
            }
        }

        if (in_array($staticPath, $urlMenu)) {
            $this->authorize($action . ' ' . $staticPath);
        }

        return parent::callAction($method, $parameters);
    }
}
