<?php

namespace Database\Seeders;

use App\Models\DataKaryawan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DataKaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /**
         * @var DataKaryawan $dataKaryawan
         */

        $dataKaryawan = DataKaryawan::create([
            'name'              => 'Febri Hardian',
            'phone'             => '089609892700',
            'email'             => 'febrihardiansastra@gmail.com',
            'password'          => bcrypt('password'),
        ]);

        // $dataKaryawan->jabatans()->create(['jabatan' => 'Backend Developer']);
        // $dataKaryawan->tempat_kerjas()->create(['tempat_kerja' => 'Sukabumi']);
    }
}
