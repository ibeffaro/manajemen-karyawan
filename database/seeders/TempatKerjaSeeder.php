<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterData\TempatKerja;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TempatKerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TempatKerja::create(['tempat_kerja' => 'Sukabumi']);
    }
}
