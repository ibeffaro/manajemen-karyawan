<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user   = [
            'superadmin', 'adminhr', 'karyawan'
        ];
        $default = [
            'password'          => bcrypt('password'),
            'email_verified_at' => now(),
            'remember_token'    => Str::random(10)
        ];
        foreach ($user as $val) {
            User::create([...$default, ...[
                'name'  => $val,
                'email' => $val . '@gmail.com',
            ]])->assignRole($val);
        }
    }
}
