<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Konfigurasi\Menu;
use App\Traits\HasMenuPermission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MenuSeeder extends Seeder
{
    use HasMenuPermission;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /**
         * @var Menu $mainMenu
         */

        // Konfigurasi
        $mainMenu = Menu::firstOrCreate(['url' => 'konfigurasi'], ['name' => 'Konfigurasi', 'icon' => 'fa-cog', 'orders' => 10]);
        $this->attachMenuPermission($mainMenu, ['read'], ['superadmin']);
        $subMenu = $mainMenu->subMenus()->create(['name' => 'User List', 'url' => $mainMenu->url . '/user-list', 'icon' => 'fa-users', 'orders' => 1]);
        $this->attachMenuPermission($subMenu, ['read', 'create', 'update', 'delete'], ['superadmin']);
        $subMenu = $mainMenu->subMenus()->create(['name' => 'Menu', 'url' => $mainMenu->url . '/menu', 'icon' => 'fa-bars', 'orders' => 1]);
        $this->attachMenuPermission($subMenu, ['read', 'create', 'update', 'delete'], ['superadmin']);
        $subMenu = $mainMenu->subMenus()->create(['name' => 'Role', 'url' => $mainMenu->url . '/roles', 'icon' => 'fa-critical-role', 'orders' => 2]);
        $this->attachMenuPermission($subMenu, ['read', 'create', 'update', 'delete'], ['superadmin']);
        $subMenu = $mainMenu->subMenus()->create(['name' => 'Akses Role', 'url' => $mainMenu->url . '/akses-role', 'icon' => 'fa-universal-access', 'orders' => 4]);
        $this->attachMenuPermission($subMenu, ['read', 'update'], ['superadmin']);

        // Master Data
        $mainMenu = Menu::firstOrCreate(['url' => 'master-data'], ['name' => 'Master Data', 'icon' => 'fa-database', 'orders' => 9]);
        $this->attachMenuPermission($mainMenu, ['read'], ['superadmin']);
        $subMenu = $mainMenu->subMenus()->create(['name' => 'Jabatan', 'url' => $mainMenu->url . '/jabatan', 'icon' => 'fa-check', 'orders' => 1]);
        $this->attachMenuPermission($subMenu, ['read', 'create', 'update', 'delete'], ['superadmin']);
        $subMenu = $mainMenu->subMenus()->create(['name' => 'Tempat Kerja', 'url' => $mainMenu->url . '/tempat-kerja', 'icon' => 'fa-map', 'orders' => 2]);
        $this->attachMenuPermission($subMenu, ['read', 'create', 'update', 'delete'], ['superadmin']);

        // Data Karyawan
        $mainMenu = Menu::firstOrCreate(['url' => 'data-karyawan'], ['name' => 'Data Karyawan', 'icon' => 'fa-user', 'orders' => 1]);
        $this->attachMenuPermission($mainMenu, ['read', 'create', 'update', 'delete'], ['superadmin']);
    }
}
