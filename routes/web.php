<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\DataKaryawanController;
use App\Http\Controllers\Konfigurasi\MenuController;
use App\Http\Controllers\Konfigurasi\RoleController;
use App\Http\Controllers\MasterData\JabatanController;
use App\Http\Controllers\Konfigurasi\UserListController;
use App\Http\Controllers\Konfigurasi\AksesRoleController;
use App\Http\Controllers\MasterData\TempatKerjaController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::group(['prefix' => 'konfigurasi', 'as' => 'konfigurasi.'], function () {
        Route::resource('user-list', UserListController::class)->parameters(['user-list' => 'user']);
        Route::put('menu/sort', [MenuController::class, 'sort'])->name('menu.sort');
        Route::resource('menu', MenuController::class);
        Route::resource('roles', RoleController::class);
        Route::get('akses-role/{role}/role', [AksesRoleController::class, 'getPermissionByRole']);
        Route::resource('akses-role', AksesRoleController::class)->except(['create', 'store', 'delete'])->parameters(['akses-role' => 'role']);
    });

    Route::group(['prefix' => 'master-data', 'as' => 'master-data.'], function () {
        Route::resource('jabatan', JabatanController::class);
        Route::resource('tempat-kerja', TempatKerjaController::class);
    });

    Route::resource('data-karyawan', DataKaryawanController::class);
});

require __DIR__ . '/auth.php';
