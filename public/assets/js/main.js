var varHandleCheckMenu = false;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// select2 lang
!(function () {
    if ($().select2) {
        if (jQuery && jQuery.fn && jQuery.fn.select2 && jQuery.fn.select2.amd) var n = jQuery.fn.select2.amd;
        n.define("select2/i18n/app", [], function () {
            return {
                errorLoading: function () {
                    return lang.select2_errorLoading;
                },
                inputTooLong: function (n) {
                    return lang.select2_inputTooLong.replace('{replace}', (n.input.length - n.maximum));
                },
                inputTooShort: function (n) {
                    return lang.select2_inputTooShort.replace('{replace}', (n.minimum - n.input.length));
                },
                loadingMore: function () {
                    return lang.select2_loadingMore;
                },
                maximumSelected: function (n) {
                    return lang.select2_maximumSelected.replace('{replace}', n.maximum);
                },
                noResults: function () {
                    return lang.select2_noResults;
                },
                searching: function () {
                    return lang.select2_searching;
                },
                removeAllItems: function () {
                    return lang.select2_removeAllItems;
                },
            };
        }),
            n.define,
            n.require;
    }
})();

var cAlert = cAlert || (function ($) {
    'use strict';
    return {
        open: function (message, status, onClick, paramClick) {
            if (typeof message === 'undefined') {
                message = 'Hello World';
            }
            var title = 'Announcement';
            var type = 'info';
            if (typeof status !== 'undefined') {
                if (inArray(status, ['error', 'danger'])) {
                    title = 'Error';
                    type = 'error';
                } else if (status == 'failed') {
                    title = 'Failed';
                    type = 'error';
                } else if (status == 'success') {
                    title = 'Success';
                    type = 'success';
                } else if (status == 'warning') {
                    title = 'Warning';
                    type = 'warning';
                }
            }
            if (typeof swal !== 'undefined') {
                swal({
                    title: title,
                    text: message,
                    icon: type,
                    button: 'Ok',
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                }).then((value) => {
                    if (typeof onClick != 'undefined' && onClick != null) {
                        var parseAct = onClick.split(':', 2);
                        var act = window[parseAct[0]];
                        if (typeof act == 'function') {
                            if (typeof parseAct[1] == 'undefined') {
                                if (typeof paramClick != 'undefined') {
                                    act(paramClick);
                                } else {
                                    act();
                                }
                            } else if (typeof parseAct[1] != 'undefined' && typeof paramClick == 'object') {
                                if (typeof paramClick[parseAct[1]] != 'undefined') {
                                    act(paramClick[parseAct[1]]);
                                } else {
                                    act(parseAct[1]);
                                }
                            } else {
                                act(onClick.replace(parseAct[0] + ':', ''));
                            }
                        }
                    }
                });
            } else {
                alert(message);
            }
        }
    }
})(jQuery);
var cConfirm = cConfirm || (function ($) {
    'use strict';
    return {
        open: function (message, onConfirm, paramConfirm, tipe) {
            if (typeof message === 'undefined') {
                message = 'Are you sure?';
            }
            var title = 'Confirmation';
            var type = 'question';
            if (typeof tipe !== 'undefined') type = tipe;
            if (tipe == 'warning') title = 'Warning';
            if (typeof swal !== 'undefined') {
                swal({
                    title: title,
                    text: message,
                    icon: type,
                    buttons: {
                        cancel: 'Cancel',
                        catch: {
                            text: 'Continue',
                            value: "catch",
                            closeModal: true
                        }
                    },
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                }).then((value) => {
                    if (value == "catch") {
                        $('.swal-button--cancel,.swal-button--catch').attr('disabled', true);
                        setTimeout(function () {
                            if (typeof onConfirm !== 'undefined') {
                                var parseAct = onConfirm.split(':', 2);
                                var act = window[parseAct[0]];
                                if (typeof act == 'function') {
                                    if (typeof parseAct[1] == 'undefined') {
                                        if (typeof paramConfirm != 'undefined') {
                                            act(paramConfirm);
                                        } else {
                                            act();
                                        }
                                    } else {
                                        act(onConfirm.replace(parseAct[0] + ':', ''));
                                    }
                                }
                            } else {
                                cAlert.open('Continue', 'success');
                            }
                            // }, 300);
                        });
                    } else {
                        swal.close();
                    }
                });
            } else {
                if (confirm(message)) {
                    if (typeof onConfirm !== 'undefined' && onConfirm != null) {
                        var parseAct = onConfirm.split(':', 2);
                        var act = window[parseAct[0]];
                        if (typeof act == 'function') {
                            if (typeof parseAct[1] == 'undefined') {
                                if (typeof paramConfirm != 'undefined') {
                                    act(paramConfirm);
                                } else {
                                    act();
                                }
                            } else {
                                act(onConfirm.replace(parseAct[0] + ':', ''));
                            }
                        }
                    } else {
                        cAlert.open('Continue', 'success');
                    }
                }
            }
        }
    }
})(jQuery);
var cModal = cModal || (function ($) {
    'use strict';
    var $dialog = $('<div class="modal fade" aria-hidden="true" tabindex="-1"><div class="modal-dialog modal-dialog-scrollable"><div class="modal-content"><div class="modal-header"><h5 class="modal-title"></h5><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button></div><div class="modal-body"></div></div></div></div>');
    return {
        open: function (header, body, size) {
            var title = 'Modal';
            var subtitle = '';
            var modalSize = '';
            if (typeof header == 'object') {
                if (typeof header[0] != 'undefined') title = header[0];
                if (typeof header[1] != 'undefined') subtitle = header[1];
            } else {
                title = header;
            }
            if (subtitle != '') title += '<small>' + subtitle + '</small>';
            if (typeof size != 'undefined') {
                if (size.indexOf('modal-') == -1) modalSize = 'modal-' + size;
                else modalSize = size;
            }
            $dialog.find('.modal-body').html(body);
            $dialog.find('.modal-header').children('.modal-title').html(title);
            if (modalSize) {
                $dialog.find('.modal-dialog').addClass(modalSize);
            } else {
                $dialog.find('.modal-dialog').attr('class', 'modal-dialog modal-dialog-scrollable');
            }
            var modalGenerated = new bootstrap.Modal($dialog[0]);
            modalGenerated.show();
        }
    };
})(jQuery);
var cToast = cToast || (function ($) {
    'use strict';
    var $dialog = $(
        '<div class="toast align-items-center text-white hide border-0 mt-2" role="alert" aria-live="assertive" aria-atomic="true"><div class="d-flex">' +
        '<div class="toast-body"></div><button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button></div></div>'
    );
    return {
        open: function (message, status) {
            var idDialog = 'toast-' + rand();
            if (typeof message === 'undefined') {
                message = 'Response Invalid';
                // message = 'Hello World';
            }
            var type = 'primary';
            if (typeof status !== 'undefined') {
                if (inArray(status, ['error', 'danger', 'failed'])) {
                    type = 'danger';
                } else if (status == 'info') {
                    type = 'info';
                } else if (status == 'success') {
                    type = 'success';
                } else if (status == 'warning') {
                    type = 'warning';
                }
            }
            $dialog.attr('id', idDialog);
            $dialog.find('.toast-body').html(message);
            $dialog.removeClass('bg-success').removeClass('bg-primary').removeClass('bg-info').removeClass('bg-warning').removeClass('bg-danger');
            $dialog.addClass('bg-' + type);

            if ($('#app-toast-lists').length == 0) {
                $('body').append('<div class="position-fixed bottom-0 end-0 p-3" id="app-toast-lists" style="z-index: 3000"></div>');
            }
            $('#app-toast-lists').prepend($dialog[0].outerHTML);
            var myToastEl = document.getElementById(idDialog);
            var myToast = new bootstrap.Toast(myToastEl);
            myToast.show();
            myToastEl.addEventListener('hidden.bs.toast', function () {
                $('#' + idDialog).remove();
                if ($('#app-toast-lists .toast').length == 0) {
                    $('#app-toast-lists').remove();
                }
            });
        }
    };
})(jQuery);

function select2TemplateResult(result) {
    return result.text;
}
function select2TemplateSelection(selection) {
    return selection.text;
}

function select2Init(parent) {
    var select2Element = $('.select2');
    if (typeof parent != 'undefined') {
        select2Element = parent.find('.select2');
    }
    console.log(select2Element);
    if ($().select2) {
        select2Element.each(function () {
            var $t = $(this);

            if ($t.data('select2')) {
                $t.select2('destroy');
            }

            var width = '100%';
            var parent = $('body');
            if ($(this).closest('.modal').length > 0) {
                parent = $(this).closest('.modal');
            }
            if (typeof $t.attr('data-width') != 'undefined') {
                width = $t.attr('data-width');
            }
            if (!width) width = 'resolve';

            var templateResult = select2TemplateResult;
            var templateSelection = select2TemplateSelection;

            if ($t.attr('id') != undefined) {
                var fn = window[$t.attr('id') + 'TemplateResult'];
                if (typeof fn == 'function') {
                    templateResult = fn;
                }

                var fns = window[$t.attr('id') + 'TemplateSelection'];
                if (typeof fns == 'function') {
                    templateSelection = fns;
                }
            }

            var config = {
                language: 'app',
                width: width,
                dropdownParent: parent,
                templateResult: templateResult,
                templateSelection: templateSelection
            };

            if (typeof $t.attr('data-search') != 'undefined' && $t.attr('data-search').toLowerCase() == 'false') {
                config.minimumResultsForSearch = -1;
            }
            if (typeof $t.attr('placeholder') != 'undefined' && $t.attr('placeholder') != '') {
                config.placeholder = $t.attr('placeholder');
            }
            $t.select2(config);
        });
    }
}

$(document).ready(function () {
    showLoader(false)
    select2Init();
    handleCheckMenu()
    $('#left-panel .modal, #main-panel .modal, #right.panel.modal').each(function () {
        $(this).find(':input').each(function () {
            if ($(this).data('select2')) {
                $(this).select2('destroy');
            }
            if ($(this).siblings('.bootstrap-tagsinput').length > 0) {
                $(this).tagsinput('destroy');
            }
        });
        $('body').append(this.outerHTML);
        $(this).remove();
        showLoader(false)
        select2Init();
        handleCheckMenu()
    });
});

$(document).on('select2:open', function () {
    setTimeout(function () {
        if ($('.select2-container--open').find('.select2-selection--single').length > 0 && !$('.select2-search__field').parent().hasClass('select2-search--hide')) {
            $('.select2-search__field')[0].focus();
        }
    }, 100);
});

$(document).on('change', '[data-validation]', function () {
    if (($(this).hasClass('is-invalid') || $(this).parent().parent().find('span.msg-invalid-form').length > 0) && $(this).val() != '') {
        $(this).removeClass('is-invalid');
        if ($(this).parent().hasClass('input-group')) {
            $(this).parent().parent().find('span.msg-invalid-form').remove();
        } else {
            $(this).parent().find('span.msg-invalid-form').remove();
            if ($(this).parent().children('span.select2').length == 1) {
                $(this).parent().children('span.select2').removeClass('is-invalid');
            }
        }
    }
});

// Penambahan laranub

function showLoader(show = true) {
    const preloader = $(".preloader");

    if (show) {
        preloader.css({
            opacity: 1,
            visibility: "visible",
        });
    } else {
        preloader.css({
            opacity: 0,
            visibility: "hidden",
        });
    }
}

function submitLoader(formId = 'form-action') {
    const button = $(formId).find('button[type="submit"]');
    function show() {
        button.addClass('btn-load')
            .attr('disabled', true)
            .html(`<span class="d-flex align-items-center"><span class="spinner-border flex-shrink-0"></span><span class="flex-grow-1 ms-2">Loading...</span></span>`);
    }

    function hide(text = "Save") {
        button.removeClass('btn-load').removeAttr('disabled').text(text);
    }

    return {
        show,
        hide
    }
}

function handleFormSubmit(selector) {

    function init() {
        const _this = this
        $(selector).on('submit', function (e) {
            e.preventDefault()

            const _form = this
            $.ajax({
                url: this.action,
                method: this.method,
                data: new FormData(_form),
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $(_form).find('.is-invalid').removeClass('is-invalid')
                    $(_form).find('.invalid-feedback').remove()
                    submitLoader().show()
                },
                success: (res) => {
                    if (_this.runDefaultSuccessCallback) {
                        $('#modal-add').modal('hide')
                        cToast.open(res.message, res.status);
                    }
                    _this.onSuccessCallback && _this.onSuccessCallback(res)
                    _this.setDataTableId && window.LaravelDataTables[_this.setDataTableId].ajax.reload(null, false)

                },
                complete: function () {
                    submitLoader().hide()
                },
                error: function (err) {
                    const errors = err.responseJSON?.errors;
                    if (errors) {
                        for (let [key, message] of Object.entries(errors)) {
                            $(`[name=${key}]`).addClass('is-invalid')
                                .parent()
                                .append(`<div class="invalid-feedback">${message}</div>`)
                        }
                    }

                    cToast.open(err.responseJSON?.message, 'error');
                    // cAlert.open(err.responseJSON?.message, 'error');
                }
            })
        })
    }

    function onSuccess(cb, runDefault = true) {
        this.onSuccessCallback = cb
        this.runDefaultSuccessCallback = runDefault
        return this
    }

    function setDataTable(id) {
        this.setDataTableId = id
        return this
    }

    return {
        init,
        runDefaultSuccessCallback: true,
        onSuccess,
        setDataTable
    }
}

function handleAjax(url, method = 'get') {

    function onSuccess(cb, runDefault = true) {
        this.onSuccessCallback = cb
        this.runDefaultSuccessCallback = runDefault
        return this
    }

    function execute() {
        $.ajax({
            url,
            method,
            beforeSend: function () {
                showLoader()
            },
            complete: function () {
                showLoader(false)
            },
            success: (res) => {
                if (this.runDefaultSuccessCallback) {
                    const modal = $('#modal-add')
                    modal.html(res)
                    modal.modal('show')
                }
                this.onSuccessCallback && this.onSuccessCallback(res)
            },
            error: function (err) {
                console.log(err)
            }
        })
    }

    function onError(cb) {
        this.onErrorCallback = cb
        return this
    }

    return {
        execute,
        onSuccess,
        runDefaultSuccessCallback: true
    }
}

// function handleCheckMenu() {
//     $(document).on('click', '.cb-role-all', function () {
//         const childs = $(this).parents('tr').find('.cb-role-child')
//         childs.prop('checked', this.checked)
//     })

//     $(document).on('click', '.cb-role-child', function () {
//         const parent = $(this).parents('tr')
//         const childs = parent.find('.cb-role-child')
//         const checked = parent.find('.cb-role-child:checked')
//         parent.find('.cb-role-all').prop('checked', childs.length == checked.length)
//     })

//     $('.cb-role-all').each(function () {
//         const parent = $(this).parents('tr')
//         const childs = parent.find('.cb-role-child')
//         const checked = parent.find('.cb-role-child:checked')
//         parent.find('.cb-role-all').prop('checked', childs.length == checked.length)
//     })
// }

function checkCb(e) {
    var cbAll = e.parents('tr').find('.cb-role-child').length;
    var cbChecked = e.parents('tr').find('.cb-role-child:checked').length;
    var el = e.parents('tr').find('.cb-role-all');
    if (cbChecked == 0) {
        el.prop('indeterminate', false);
        el.prop('checked', false);
    } else if (cbAll == cbChecked) {
        el.prop('indeterminate', false);
        el.prop('checked', true);
    } else {
        el.prop('indeterminate', true);
        el.prop('checked', false);
    }
}

function handleCheckMenu() {
    $(document).on('click', '.cb-role-all', function () {
        if ($(this).is(':checked')) {
            $(this).parents('tr').find('.cb-role-child').prop('checked', true);
        } else {
            $(this).parents('tr').find('.cb-role-child').prop('checked', false);
        }
    });
    $(document).on('click', '.cb-role-child', function () {
        var $t = $(this);
        $t.parents('tr').find('.cb-role-child').each(function () {
            if ($(this).attr('name').indexOf('view') != -1) {
                $t = $(this);
            }
        });
        if ($(this).is(':checked') && !$t.is(':checked')) {
            $t.prop('checked', true);
        } else if ($(this).attr('name').indexOf('view') != -1 && !$(this).is(':checked')) {
            $(this).parents('tr').find('.cb-role-child').prop('checked', false);
        }
        checkCb($(this));
    });
    $('.cb-role-all').each(function () {
        const parent = $(this).parents('tr')
        const childs = parent.find('.cb-role-child')
        const checked = parent.find('.cb-role-child:checked')
        parent.find('.cb-role-all').prop('checked', childs.length == checked.length)
    })
}

$(document).on('click', '.btn-add', function (e) {
    e.preventDefault()

    handleAjax(this.href).onSuccess(function (res) {
        handleFormSubmit('#form-action')
            .setDataTable($('#datatable').attr('data-table'))
            .init()
        if (varHandleCheckMenu) {
            handleCheckMenu()
        }
    }).execute()
})

$(document).on('click', '.btn-delete', function (e) {
    e.preventDefault()
    cConfirm.open('Are you sure you delete this data?', '__deleteData', {
        'href': this.href
    });
});

function __deleteData(r) {
    handleAjax(r.href, 'delete')
        .onSuccess(function (res) {
            cToast.open(res.message, res.status);
            window.LaravelDataTables[$('#datatable').attr('data-table')].ajax.reload(null, false)
        }, false).execute()
}
