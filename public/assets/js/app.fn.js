var xhrAll = {};
var base_url = '';
var max_upload_filesize = 0;
var modalList = {};
var shortcutDelay = null;
var e = '';


function validation(e) {
	var valid = true;
	e.find('[data-validation]:enabled').each(function () {
		var $t = $(this);
		var validate = $t.attr('data-validation');
		var spl = validate.split('|');
		var m = '';
		if ($(this).closest('.hidden').length == 0) {
			$.each(spl, function (i, d) {
				var v = d.split(':');
				var t = v[0];
				var l = typeof v[1] == 'undefined' ? 0 : parseFloat(v[1]);
				var n = getInputLabel($t);
				var vl = $t.val();
				var int_v = vl == null || $.isArray(vl) ? '' : vl.replaceAll('.', '').replaceAll(',', '.');
				var int_val = parseFloat(int_v);
				if (isNaN(l)) l = v[1];
				var req_val = typeof $t.val() == 'string' ? $t.val().trim() : $t.val();
				if (t == 'required' && (($t.val() != null && req_val.length == 0) || $t.val() == null) && m == '') {
					m = '<strong>' + n + '</strong> ' + lang.harus_diisi;
				}
				else if (t == 'strong_password' && m == '' && ($t.val() != null && $t.val().length != 0)) {
					var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/;
					if (!re.test($t.val())) {
						m = '<strong>' + n + '</strong> ' + lang.msg_strong_password;
					}
				}
				else if (t == 'email' && m == '' && ($t.val() != null && $t.val().length != 0)) {
					var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
					if (!re.test($t.val())) {
						m = '<strong>' + n + '</strong> ' + lang.harus_diisi_format_email + ' (ex@email.xx)';
					}
				}
				else if (t == 'phone' && m == '' && ($t.val() != null && $t.val().length != 0)) {
					var re = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
					if (!re.test($t.val())) {
						m = '<strong>' + n + '</strong> ' + lang.harus_diisi_format_nomor_telepon;
					}
				}
				else if (t == 'numeric' && m == '' && ($t.val() != null && $t.val().length != 0)) {
					var re = /^[0-9.,]+$/;
					if (!re.test($t.val())) {
						m = '<strong>' + n + '</strong> ' + lang.harus_diisi_format_angka;
					}
				}
				else if (t == 'letter' && m == '' && ($t.val() != null && $t.val().length != 0)) {
					var re = /^[a-zA-Z_\-]+$/;
					if (!re.test($t.val())) {
						m = '<strong>' + n + '</strong> ' + lang.harus_diisi_format_huruf;
					}
				}
				else if (t == 'alphanumeric' && m == '' && ($t.val() != null && $t.val().length != 0)) {
					var re = /^[0-9a-zA-Z_\-]+$/;
					if (!re.test($t.val())) {
						m = '<strong>' + n + '</strong> ' + lang.harus_diisi_format_huruf_atau_angka;
					}
				}
				else if (t == 'username' && m == '' && ($t.val() != null && $t.val().length != 0)) {
					var re = /^[0-9a-zA-Z.,_\-]+$/;
					if (!re.test($t.val())) {
						m = '<strong>' + n + '</strong> Format tidak valid';
					}
				}
				else if (t == 'length' && $t.val().length != l && m == '' && ($t.val() != null && $t.val().length != 0)) {
					m = '<strong>' + n + '</strong> ' + lang.harus + ' ' + l + ' ' + lang.karakter;
				}
				else if (t == 'min-length' && $t.val().length < l && m == '' && ($t.val() != null && $t.val().length != 0)) {
					m = '<strong>' + n + '</strong> ' + lang.minimal + ' ' + l + ' ' + lang.karakter;
				}
				else if (t == 'max-length' && $t.val().length > l && m == '' && ($t.val() != null && $t.val().length != 0)) {
					m = '<strong>' + n + '</strong> ' + lang.maksimal + ' ' + l + ' ' + lang.karakter;
				}
				else if (t == 'equal' && m == '' && $t.closest('form').find('[name="' + l + '"]').length == 1 && $t.val() != $t.closest('form').find('[name="' + l + '"]').val()) {
					if (typeof $t.closest('form').find('[name="' + l + '"]').closest('.form-group').attr('class') != 'undefined') {
						m = '<strong>' + n + '</strong> ' + lang.tidak_cocok_dengan + ' ' + $t.closest('form').find('[name="' + l + '"]').closest('.form-group').children('label').text();
					} else if (typeof $t.closest('form').find('[name="' + l + '"]').attr('aria-label') != 'undefined') {
						m = '<strong>' + n + '</strong> ' + lang.tidak_cocok_dengan + ' ' + $t.closest('form').find('[name="' + l + '"]').attr('aria-label');
					} else if (typeof $('label[for="' + l + '"]').attr('for') != 'undefined') {
						m = '<strong>' + n + '</strong> ' + lang.tidak_cocok_dengan + ' ' + $('label[for="' + l + '"]').text();
					} else {
						m = '<strong>' + n + '</strong> ' + lang.tidak_cocok_dengan + ' ' + l;
					}
				}
				else if (t == 'min' && !isNaN(int_val) && int_val < l) {
					var lView = $t.hasClass('input-currency') ? customFormat(l) : l;
					if ($t.hasClass('input-currency') && $t.attr('data-decimal') != undefined && !isNaN(parseInt($t.attr('data-decimal')))) {
						lView = numberFormat(l, parseInt($t.attr('data-decimal')), ',', '.');
					}
					m = '<strong>' + n + '</strong> ' + lang.tidak_boleh_kurang_dari + ' ' + lView;
				}
				else if (t == 'max' && !isNaN(int_val) && int_val > l) {
					var lView = $t.hasClass('input-currency') ? customFormat(l) : l;
					if ($t.hasClass('input-currency') && $t.attr('data-decimal') != undefined && !isNaN(parseInt($t.attr('data-decimal')))) {
						lView = numberFormat(l, parseInt($t.attr('data-decimal')), ',', '.');
					}
					m = '<strong>' + n + '</strong> ' + lang.tidak_boleh_lebih_dari + ' ' + lView;
				}
				if (!m && $t.attr('min') != undefined) {
					var min = parseFloat($t.attr('min'));
					if (!isNaN(int_val) && int_val < min) {
						var lView = $t.hasClass('input-currency') ? customFormat(min) : min;
						if ($t.hasClass('input-currency') && $t.attr('data-decimal') != undefined && !isNaN(parseInt($t.attr('data-decimal')))) {
							lView = numberFormat(min, parseInt($t.attr('data-decimal')), ',', '.');
						}
						m = '<strong>' + n + '</strong> ' + lang.tidak_boleh_kurang_dari + ' ' + lView;
					}
				}
				if (!m && $t.attr('max') != undefined) {
					var max = parseFloat($t.attr('max'));
					if (!isNaN(int_val) && int_val > max) {
						var lView = $t.hasClass('input-currency') ? customFormat(max) : max;
						if ($t.hasClass('input-currency') && $t.attr('data-decimal') != undefined && !isNaN(parseInt($t.attr('data-decimal')))) {
							lView = numberFormat(max, parseInt($t.attr('data-decimal')), ',', '.');
						}
						m = '<strong>' + n + '</strong> ' + lang.tidak_boleh_kurang_dari + ' ' + lView;
					}
				}
			});
			if (m) {
				valid = false;
				if ($t.parent().hasClass('input-group')) {
					if ($t.parent().parent().find('span.msg-invalid-form').length == 0) {
						$t.addClass('is-invalid');
						$t.parent().parent().append('<span class="msg-invalid-form headShake">' + m + '</span>');
						setTimeout(function () {
							$t.parent().parent().find('.msg-invalid-form').removeClass('headShake');
						}, 500);
					} else {
						$t.parent().parent().find('.msg-invalid-form').addClass('headShake');
						setTimeout(function () {
							$t.parent().parent().find('.msg-invalid-form').removeClass('headShake');
						}, 500);
					}
				} else {
					if ($t.parent().find('span.msg-invalid-form').length == 0) {
						$t.addClass('is-invalid');
						if ($t.parent().children('span.select2').length == 1) {
							$t.parent().children('span.select2').addClass('is-invalid');
						}
						$t.parent().append('<span class="msg-invalid-form headShake">' + m + '</span>');
						setTimeout(function () {
							$t.parent().find('.msg-invalid-form').removeClass('headShake');
						}, 500);
					} else {
						$t.parent().find('.msg-invalid-form').addClass('headShake');
						setTimeout(function () {
							$t.parent().find('.msg-invalid-form').removeClass('headShake');
						}, 500);
					}
				}
			}
		}
	});
	if (!valid) {
		e.find('.is-invalid').first().focus();
		if (e.find('.tab-content').length > 0) {
			var paneTarget = e.find('.is-invalid').first().closest('.tab-pane').attr('id');
			e.find('.tab-pane').removeClass('active').removeClass('show');
			e.find('.nav-link').removeClass('active');
			$('#' + paneTarget).addClass('active').addClass('show');
			$('[data-bs-target="#' + paneTarget + '"]').addClass('active');
		}
		return false;
	} else {
		return true;
	}
}

$(document).on('change', '[data-validation]', function () {
	if (($(this).hasClass('is-invalid') || $(this).parent().parent().find('span.msg-invalid-form').length > 0) && $(this).val() != '') {
		$(this).removeClass('is-invalid');
		if ($(this).parent().hasClass('input-group')) {
			$(this).parent().parent().find('span.msg-invalid-form').remove();
		} else {
			$(this).parent().find('span.msg-invalid-form').remove();
			if ($(this).parent().children('span.select2').length == 1) {
				$(this).parent().children('span.select2').removeClass('is-invalid');
			}
		}
	}
});

$(document).on('keyup', '[data-validation]', function () {
	$(this).trigger('change');
});

$(document).on('keypress', 'input.text-numeric', function (e) {
	var wh = e.which;
	if (e.shiftKey) {
		if (wh == 0) return true;
	}
	if (e.metaKey || e.ctrlKey) {
		if (wh == 86 || wh == 118) {
			$(this)[0].onchange = function () {
				$(this)[0].value = $(this)[0].value.replace(/[^0-9]/g, '');
			}
		}
		return true;
	}
	if (wh == 0 || wh == 8 || wh == 13 || wh == 32 || wh == 44 || wh == 46 || (48 <= wh && wh <= 57))
		return true;
	return false;
});

function modal(id) {
	if (typeof modalList[id] == 'undefined') {
		modalList[id] = new bootstrap.Modal(document.getElementById(id), {
			keyboard: false,
			backdrop: 'static'
		});

		var mdl = document.getElementById(id);
		mdl.addEventListener('shown.bs.modal', function (event) {
			$('#' + id).find(':input:not([disabled]):not([readonly]):not([type=hidden]):not(button)').first().focus();
		});
	}
	return modalList[id];
}
function closeModal() {
	$('.modal.show').each(function () {
		modal($(this).attr('id')).hide();
	});
}
function baseURL(string) {
	if (typeof string != "string") string = '';
	return base_url + string;
}
function rand(min, max) {
	if (typeof min != 'undefined' && typeof max != 'undefined') {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	} else {
		return Math.floor(Math.random() * (9999 - 1000 + 1)) + 100;
	}
}
function pregMatchAll(regex, string) {
	var rx = new RegExp(regex);
	var txt = string;
	var mtc0 = [];
	var mtc1 = [];
	while ((match = rx.exec(txt)) != null) {
		mtc0.push(match[0]);
		mtc1.push(match[1].trim());
		txt = txt.replace(match[0]);
	}
	return [mtc0, mtc1];
}
function reload() {
	window.location.reload();
}
function numberFormat(e, c, d, t, z) {
	var n = e,
		c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
		j = (j = i.length) > 3 ? j % 3 : 0;
	x = z == 'absolute' ? false : true;
	var result = s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	return result;
}
function customFormat(str) {
	return numberFormat(str, 0, ',', '.');
}
function moneyToNumber(str) {
	if (str == '') {
		return 0;
	} else {
		return parseFloat(str.replace(/\./g, '').replaceAll(',', '.'));
	}
}
function toNumber(str) {
	str = str.replace(',', '.');
	if (str == '' || isNaN(str)) {
		return 0;
	} else {
		return parseFloat(str);
	}
}
function curDate() {
	var d = new Date,
		dformat = [d.getFullYear(),
		(d.getMonth() + 1).padLeft(),
		d.getDate().padLeft()].join('-') + ' ' +
			[d.getHours().padLeft(),
			d.getMinutes().padLeft(),
			d.getSeconds().padLeft()].join(':');
	return dformat;
}
Number.prototype.padLeft = function (base, chr) {
	var len = (String(base || 10).length - String(this).length) + 1;
	return len > 0 ? new Array(len).join(chr || '0') + this : this;
}
function customDate(e, timeFormat) {
	var dt = e;
	if (e.length == 10) {
		if (e == '0000-00-00' || e == null) {
			dt = '';
		} else {
			var x = dt.split('-');
			if (x.length == 3) {
				dt = x[2] + '/' + x[1] + '/' + x[0];
			}
		}
	} else if (e.length == 19) {
		if (e == '0000-00-00 00:00:00' || e == null) {
			dt = '';
		} else {
			var x = dt.split(' ');
			if (x.length == 2) {
				var y = x[0].split('-');
				if (y.length == 3) {
					var dt = y[2] + '/' + y[1] + '/' + y[0];
					if (typeof timeFormat == 'undefined') {
						timeFormat = 'h:m';
					} else {
						timeFormat = timeFormat.toLowerCase();
					}
					tt = x[1].split(':');
					if (timeFormat == 'h') {
						dt += ' ' + tt[0];
					} else if (timeFormat == 'h:m') {
						dt += ' ' + tt[0] + ':' + tt[1];
					} else if (timeFormat == 'h:m:s') {
						dt += ' ' + tt[0] + ':' + tt[1] + ':' + tt[2];
					}
				}
			}
		}
	}
	return dt;
}
function cPercent(str) {
	var x = str.split('.');
	var result = '';
	if (x.length == 2) {
		if (parseInt(x[1]) == 0) {
			result = x[0];
		} else {
			result = str.replace('.', ',');
		}
	}
	return result;
}
function encodeId(e, encode_key) {
	if (typeof encode_key == 'undefined') {
		encode_key = $('meta[name="app-number"]').attr('content');
	}
	var hashids_init = new Hashids(encode_key);
	var id = parseInt(e);
	var encode_id = '';
	if (typeof e == 'object') {
		encode_id = hashids_init.encode(e);
	} else {
		encode_id = hashids_init.encode(id, Math.floor(Math.random() * (9999 - 1000 + 1)) + 100);
	}
	return encode_id;
}
function decodeId(e, encode_key) {
	if (typeof encode_key == 'undefined') {
		encode_key = $('meta[name="app-number"]').attr('content');
	}
	var hashids_init = new Hashids(encode_key);
	var decode_id = hashids_init.decode(e);
	return decode_id;
}
function inArray(needle, haystack) {
	var length = haystack.length;
	for (var i = 0; i < length; i++) {
		if (haystack[i] == needle) return true;
	}
	return false;
}
