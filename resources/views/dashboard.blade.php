<x-master-layout>
    <div class="panel-content p-3">
        <div class="row mb-4">
            <div class="col-lg-8">
                <div class="quick-link bg-app mb-0 h-100">
                    <div class="welcome-panel">
                        <div class="welcome-img">
                            <img src="{{ asset('assets/uploads/user/default.png') }}" alt="{{ Auth::user()->name }}" />
                        </div>
                        <div class="welcome-desc">
                            <div class="f-120 fw-bold text-uppercase mt-2">{{ Auth::user()->name }}</div>
                            <div class="f-100 mb-2">{{ Auth::user()->email }}</div>
                            <div class="btn-group mt-2">
                                <a href="{{ route('profile.edit') }}" class="btn btn-light"><i
                                        class="fa-user me-2"></i>Profil</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-none d-lg-block">
                <div class="card h-100">
                    <div class="card-body text-center">
                        <i class="fa-globe icon-circle"></i>
                        <div class="text-center f-100 mt-2">You're logged in!</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-master-layout>
