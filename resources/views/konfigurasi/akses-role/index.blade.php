<x-master-layout>
    <div class="panel-content p-3">
        <div class="card">
            <div class="card-header fw-bold"> <a href="javascript:;"> Konfigurasi</a> / Akses Role</div>
            <div class="card-body">

                <div id="datatable" data-table="role-table" class="table-responsive">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>
        <div aria-hidden="true" tabindex="-1" id="modal-add" class="modal fade"></div>
    </div>
    @push('js')
        {!! $dataTable->scripts() !!}
        <script>
            var varHandleCheckMenu = true;

            $(document).on('change', '.copy-role', function() {
                if (this.value) {
                    handleAjax(`{{ url('konfigurasi/akses-role') }}/${this.value}/role`)
                        .onSuccess(function(res) {
                            $('#menu-permissions').html(res)
                        }, false)
                        .execute()
                }
            });

            $(document).on('keyup', '#search', function() {
                const val = this.value.toLowerCase()
                $('#menu-permissions tr').show().filter(function(i, item) {
                    return item.innerText.toLowerCase().indexOf(val) == '-1'
                }).hide()
            })
        </script>
    @endpush
</x-master-layout>
