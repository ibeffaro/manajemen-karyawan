@foreach ($menu as $mm)
    <tr>
        <td>
            {{ $mm->name }}
        </td>
        <td>
            @foreach ($mm->permissions as $p)
                <div class="form-check form-check-inline form-switch">
                    <input type="checkbox" name="permission[]" id="permission-{{ $mm->id . '-' . $p->id }}"
                        class="form-check-input" aria-label="" value="{{ $p->name }}" @checked($data->hasPermissionTo($p->name))>
                    <label class="form-check-label"
                        for="permission-{{ $mm->id . '-' . $p->id }}">{{ explode(' ', Str::ucfirst($p->name))[0] }}</label>
                </div>
            @endforeach
        </td>
    </tr>
    @foreach ($mm->subMenus as $sm)
        <tr>
            <td style="padding-left: 25px;">
                <div class="form-check">
                    <input type="checkbox" id="parent{{ $mm->id . $sm->id }}" class="form-check-input cb-role-all"
                        value="1">
                    <label class="form-check-label" for="parent{{ $mm->id . $sm->id }}">
                        {{ $sm->name }}
                    </label>
                </div>
            </td>
            <td>
                @foreach ($sm->permissions as $ps)
                    <div class="form-check form-check-inline form-switch">
                        <input type="checkbox" name="permission[]" id="permission-{{ $sm->id . '-' . $ps->id }}"
                            class="form-check-input cb-role-child" aria-label="" value="{{ $ps->name }}"
                            @checked($data->hasPermissionTo($ps->name))>
                        <label class="form-check-label"
                            for="permission-{{ $sm->id . '-' . $ps->id }}">{{ explode(' ', Str::ucfirst($ps->name))[0] }}</label>
                    </div>
                @endforeach
            </td>
        </tr>
    @endforeach
@endforeach
