<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title">Akses Role
                <small class="subtitle-info">Form Data</small>
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{ $action ?? null }}" method="POST" id="form-action" autocomplete="off">
                @csrf
                @if ($data->id)
                    @method('PUT')
                @endif
                <div class="mb-3 row">
                    <div class="fw-bold text-center mb-3" style="font-size: 14px;">Role :
                        <span class="badge bg-app">
                            {{ $data->name }}
                        </span>
                    </div>
                    <div class="mb-3 row">
                        <label for="role" class="col-md-3 form-label">Copy from Role</label>
                        <div class="col-md-9">
                            <select name="role" id="role" class="form-control copy-role">
                                <option value="">Choose Role</option>
                                @foreach ($role as $r)
                                    <option value="{{ $r->id }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="search" class="col-md-3 form-label">Menu</label>
                        <div class="col-md-9">
                            <input type="text" name="search" id="search" class="form-control"
                                placeholder="Search Menu">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            </thead>
                            <tr>
                                <th width="250">Menu</th>
                                <th>Action</th>
                            </tr>
                            <thead>
                            <tbody id="menu-permissions">
                                @include('konfigurasi.akses-role.list-menu')
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="modal-footer">
                    @if ($action)
                        <button type="button" class="btn btn-theme" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-app">Save</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
