<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title">Role
                <small class="subtitle-info">Form Data</small>
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{ $action ?? null }}" method="POST" id="form-action" autocomplete="off">
                @csrf
                @if ($data->id)
                    @method('PUT')
                @endif
                <div class="mb-3 row">
                    <label for="name" class="col-md-3 required form-label">Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="name" name="name" aria-label="Menu"
                            value="{{ $data->name }}">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="icon" class="col-md-3 required form-label">Guard Name</label>
                    <div class="col-md-9">
                        <div class="input-group iconpicker-container">
                            <input type="text" class="form-control" id="guard_name" name="guard_name"
                                aria-label="Guard Name" readonly value="{{ $data->guard_name }}">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    @if ($action)
                        <button type="button" class="btn btn-theme" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-app">Save</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
