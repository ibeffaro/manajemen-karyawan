<x-master-layout>
    <div class="panel-content p-3">
        <div class="card">
            <div class="card-header fw-bold"> <a href="javascript:;"> Konfigurasi</a> / Role</div>
            <div class="card-body">
                @can('create konfigurasi/roles')
                    <div class="mb-3 appinityTable-action-button text-start">
                        <div class="btn-group" role="group">
                            <a href="{{ route('konfigurasi.roles.create') }}" class="btn btn-app btn-add"
                                app-link="default"><i class="fa-plus"></i><span>Add Role</span></a>
                        </div>
                    </div>
                @endcan

                <div id="datatable" data-table="role-table" class="table-responsive">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>
        <div aria-hidden="true" tabindex="-1" id="modal-add" class="modal fade"></div>
    </div>
    @push('js')
        {!! $dataTable->scripts() !!}
    @endpush
</x-master-layout>
