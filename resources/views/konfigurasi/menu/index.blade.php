<x-master-layout>
    <div class="panel-content p-3">
        <div class="card">
            <div class="card-header fw-bold"> <a href="javascript:;"> Konfigurasi</a> / Menu</div>
            <div class="card-body">
                @can('create konfigurasi/menu')
                    <div class="mb-3 appinityTable-action-button text-start">
                        <div class="btn-group" role="group">
                            <a href="{{ route('konfigurasi.menu.create') }}" class="btn btn-app btn-add">
                                <i class="fa-plus"></i><span>Add Menu</span>
                            </a>
                            @can('sortsss konfigurasi/menu')
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-app dropdown-toggle" data-bs-toggle="dropdown"
                                        aria-expanded="false"></button>
                                    <ul class="dropdown-menu dropdown-menu-end">
                                        <li>
                                            <a href="{{ route('konfigurasi.menu.sort') }}"
                                                class="dropdown-item dropdown-item-icon sort">
                                                <i class="fa-list"></i>Sort Menu
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endcan
                        </div>
                    </div>
                @endcan

                <div id="datatable" data-table="menu-table" class="table-responsive">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>
        <div aria-hidden="" tabindex="-1" id="modal-add" class="modal fade"></div>

        {{-- <div aria-hidden="true" tabindex="-1" id="modal-sort" class="modal fade">
            <div class="modal-dialog modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-title">Menu<small>Urutkan Menu</small></div><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer"><button type="button" class="btn btn-theme" data-bs-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-app" id="save-sort">Simpan</button>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
    @push('js')
        {!! $dataTable->scripts() !!}
        <script>
            $(document).on('change', '[name="level_menu"]', function() {
                if (this.value == 'sub_menu') {
                    $('#main-menu-wrapper').removeClass('d-none')
                } else {
                    $('#main-menu-wrapper').addClass('d-none')
                }
            });

            $(document).on('click', '.sort', function(e) {
                e.preventDefault()

                handleAjax(this.href, 'put')
                    .onSuccess(function(res) {
                        window.location.reload()
                    }, false).execute();
            });
        </script>
    @endpush
</x-master-layout>
