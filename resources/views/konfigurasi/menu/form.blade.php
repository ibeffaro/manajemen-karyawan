<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title">Menu
                <small class="subtitle-info">Form Data</small>
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            {{-- <form action="{{ $action }}" method="post" app-link="default" id="form" autocomplete="off"> --}}
            <form action="{{ $action }}" method="POST" id="form-action" autocomplete="off">
                @csrf
                @if ($data->id)
                    @method('PUT')
                @endif
                {{-- <input type="hidden" class="form-control" id="id" name="id" aria-label="" value="{{ $data->id }}"> --}}
                <div class="mb-3 row">
                    <label for="name" class="col-md-3 required form-label">Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="name" name="name" aria-label="Menu"
                            value="{{ $data->name }}">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="target" class="col-md-3 required form-label">Target URL</label>
                    <div class="col-md-9">
                        {{-- <input type="text" class="form-control" id="url" name="url" aria-label="Target URL" data-validation="required|min-length:3|letter|unique" value="{{ $data->url }}""> --}}
                        <input type="text" class="form-control" id="url" name="url" aria-label="Target URL"
                            value="{{ $data->url }}"">
                    </div>
                </div>
                <div class=" mb-3 row">
                    <label for="icon" class="col-md-3 form-label">Icon</label>
                    <div class="col-md-5">
                        <div class="input-group iconpicker-container">
                            <input type="text" class="form-control" id="icon" name="icon" aria-label="Icon"
                                value="{{ $data->icon }}">
                        </div>
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="level_menu" class="col-md-3 form-label">Level Menu</label>
                    <div class="col-md-9">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="level_menu" id="level_menu1"
                                value="main_menu" {{ !$data->parent_id ? 'checked' : '' }}>
                            <label class="form-check-label" for="level_menu1">
                                Main Menu
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="level_menu" id="level_menu2"
                                value="sub_menu" {{ $data->parent_id ? 'checked' : '' }}>
                            <label class="form-check-label" for="level_menu2">
                                Sub Menu
                            </label>
                        </div>
                    </div>
                </div>
                <div class="mb-3 row {{ !$data->parent_id ? 'd-none' : '' }}" id="main-menu-wrapper">
                    <label for="parent_id" class="col-md-3 form-label">Main Menu</label>
                    <div class="col-md-9">
                        <select name="parent_id" id="parent_id" class="form-control select2">
                            <option value="">Select Main Menu</option>
                            @foreach ($main_menu as $mm)
                                <option value="{{ $mm->id }}" {{ $data->parent_id == $mm->id ? 'selected' : '' }}>
                                    {{ $mm->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @if (request()->routeIs('konfigurasi.menu.create'))
                    <div class="mb-3 row">
                        <label for="permissions" class="col-md-3">Permissions</label>
                        <div class="col-md-9">
                            <div class="row">
                                @foreach (['read', 'create', 'update', 'delete'] as $item)
                                    <div class="mb-3 col-md-3">
                                        <div class="form-check">
                                            <input type="checkbox" name="permissions[]"
                                                id="permissions{{ $item }}" class="form-check-input"
                                                value="{{ $item }}">
                                            <label class="form-check-label"
                                                for="permissions{{ $item }}">{{ ucfirst($item) }}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                <div class="mb-3 row">
                    <label for="orders" class="col-md-3 required form-label">Order</label>
                    <div class="col-md-5">
                        {{-- <input type="text" class="form-control" id="orders" name="orders" aria-label="Urutan" data-validation="required|numeric" value="{{ $data->orders }}"> --}}
                        <input type="text" class="form-control" id="orders" name="orders" aria-label="Urutan"
                            value="{{ $data->orders }}">
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <div class="modal-footer-info">
                            <i class="fa-info-circle" data-appinity-tooltip="right"></i>
                        </div> --}}
                    <button type="button" class="btn btn-theme" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-app">Save</button>
                    {{-- <button type="submit" class="btn btn-app" form="form">Save</button> --}}
                </div>
            </form>
        </div>
    </div>
</div>
