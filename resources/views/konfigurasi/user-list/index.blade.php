<x-master-layout>
    <div class="panel-content p-3">
        <div class="card">
            <div class="card-header fw-bold"> <a href="javascript:;"> User Management</a> / User List</div>
            <div class="card-body">
                @can('create konfigurasi/user-list')
                    <div class="appinityTable-action-button text-start">
                        <div class="btn-group" role="group">
                            <a href="{{ route('konfigurasi.user-list.create') }}" class="btn btn-app btn-add"
                                app-link="default"><i class="fa-plus"></i><span>Add User</span></a>
                        </div>
                    </div>
                @endcan

                <div id="datatable" data-table="user-table" class="table-responsive">
                    {!! $dataTable->table(['class' => 'table table-stripped']) !!}
                </div>
            </div>
        </div>
        <div aria-hidden="true" tabindex="-1" id="modal-add" class="modal fade"></div>
    </div>
    @push('js')
        {!! $dataTable->scripts() !!}
    @endpush
</x-master-layout>
