<div id="left-panel">
    <div class="sidebar">
        <a href="{{ route('dashboard') }}" class="app-logo">
            <img class="img-min" src="{{ asset('assets/uploads/settings/logo.png') }}" alt="" /> <img
                src="{{ asset('assets/uploads/settings/logo.png') }}" class="img-max" alt="" />
        </a>
        <div class="panel-header">
            <div class="title" id="menu-title">&nbsp;</div>
        </div>
        <div class="list-menu">
            <ul class="top-menu">
                @foreach (menus() as $mm)
                    @can('read ' . $mm->url)
                        @if (count($mm->subMenus))
                            <li @class([
                                'have-child',
                                'open active' => str_contains(request()->path(), $mm->url),
                            ])>
                                <a href="javascript:;">
                                    <i class="{{ $mm->icon }}"></i>
                                    <span>{{ $mm->name }}</span>
                                </a>
                                <ul>
                                    @foreach ($mm->subMenus as $sm)
                                        @can('read ' . $sm->url)
                                            <li @class(['active' => str_contains(request()->path(), $sm->url)])>
                                                <a href="{{ url($sm->url) }}">
                                                    <i class="{{ $sm->icon }}"></i>
                                                    <span>{{ $sm->name }}</span>
                                                </a>
                                            </li>
                                        @endcan
                                    @endforeach
                                </ul>
                            </li>
                        @else
                            <li @class([
                                'active' => str_contains(request()->path(), $mm->url),
                            ])>
                                <a href="{{ url($mm->url) }}">
                                    <i class="{{ $mm->icon }}"></i>
                                    <span>{{ $mm->name }}</span>
                                </a>
                            </li>
                        @endif
                    @endcan
                @endforeach
                {{-- <li class="have-child">
                    <a href="/settings">
                        <i class="fa-cog"></i>
                        <span>Pengaturan</span>
                    </a>
                    <ul>
                        <li class="have-child">
                            <a href="/settings/user-management">
                                <i class="fa-user"></i>
                                <span>Manajemen Pengguna</span>
                            </a>
                            <ul>
                                <li class="">
                                    <a href="/settings/user-lists">
                                        <i class="fa-users"></i>
                                        <span>Daftar Pengguna</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="/settings/user-roles">
                                        <i class="fa-user-cog"></i>
                                        <span>Peran Pengguna</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="/settings/information">
                                <i class="fa-bullhorn"></i>
                                <span>Informasi</span>
                            </a>
                        </li>
                    </ul>
                </li> --}}
            </ul>
        </div>
    </div>
</div>
