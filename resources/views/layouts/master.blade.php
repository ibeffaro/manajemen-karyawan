<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @php
            foreach (menus() as $mm) {
                foreach ($mm->subMenus as $sm) {
                    echo str_contains(request()->path(), $sm->url) ? $sm->name : '';
                }
            }
        @endphp
    </title>
    <link rel="shortcut icon" href="{{ asset('assets/uploads/settings/title.png') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/roboto.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/fontawesome.regular.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/loading.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap5.css') }}" />
    <style type="text/css">
        :root {
            --app-border-radius: 7px;
            /* --app-color: #1d56a3; */
            --app-color: #364b7f;
            --app-color-transparent: rgba(29, 86, 163, 0.25);
            --app-color-light: #8bc4ff;
            /* --app-color-dark: #003885; */
            --app-color-dark: #283e69;
            --app-color-dark2: #002e7b;
            --app-color-text: #fff;
        }
    </style>
    @stack('css')
</head>

<body class="app-gaya4 shadow-theme app-desktop" data-theme="" data-action-pos="left">

    @include('layouts.sidebar')
    <div id="main-panel">

        @include('layouts.header')

        {{ $slot }}

    </div>
    <div class="preloader" style="visibility:hidden;">
        <div class="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    {{-- @include('layouts.right') --}}

    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.contextmenu.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2.full.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.tagsinput.js') }}"></script>
    <script src="{{ asset('assets/js/app.fn.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap5.js') }}"></script>

    @stack('js')
</body>

</html>
