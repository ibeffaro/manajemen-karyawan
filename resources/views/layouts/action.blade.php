<div class="appinityTable-action">
    {{-- @if (isset($action['detail']))
        <a href="{{ $action['detail'] }}" class="btn btn-info btn-sm btn-add" data-appinity-tooltip="top"
            data-context-key="Detail" aria-label="Detail"><i class="appinityTable-action-icon fa-info-circle"></i></a>
    @endif --}}
    @if (isset($action['edit']))
        <a href="{{ $action['edit'] }}" class="btn btn-warning btn-sm btn-add" data-appinity-tooltip="top"
            data-context-key="Edit" aria-label="Edit"><i class="appinityTable-action-icon fa-edit"></i></a>
    @endif
    @if (isset($action['delete']))
        <a href="{{ $action['delete'] }}" class="btn btn-danger btn-sm btn-delete" data-appinity-tooltip="top"
            data-context-key="delete" aria-label="Delete"><i class="appinityTable-action-icon fa-trash-alt"></i></a>
    @endif
</div>
