<div class="panel-header primary-header">
    <a href="javascript:;" class="toggle-menu ms-2"><i class="fa-bars"></i></a>
    <div class="title"></div>
    <div class="search-input">
        <input type="text" class="form-control" id="app-search-input" placeholder="Search Menu" />
        <i class="search-icon fa-search"></i>
    </div>
    {{-- <a href="javascript:;" class="toggle-right-panel" title="Informasi">
        <i class="fa-align-right"></i>
    </a> --}}
    <div class="dropdown setting-menu setting-notification">
        <a href="#" class="notification dropdown-toggle" id="dropdownNotif" data-bs-toggle="dropdown"
            aria-expanded="false">
            <i class="fa-bell"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-end p-0" aria-labelledby="dropdownNotif">
            <ul class="notification-lists">
                <li>
                    <div class="py-4 px-2 text-center"><i class="fa-bell f-lg"></i>
                        <div class="mt-2">No Notification</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="dropdown setting-menu setting-profile">
        <a href="#" class="profile dropdown-toggle" id="dropdownProfil" data-bs-toggle="dropdown"
            aria-expanded="false">
            <img src="{{ asset('assets/uploads/user/default.png') }}" alt="{{ Auth::user()->name }}"
                class="rounded-circle" />
            <div class="app-badge badge-bottom badge-success"></div>
        </a>
        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownProfil">
            <li>
                <a class="dropdown-item dropdown-item-icon" href="{{ route('profile.edit') }}">
                    <i class="fa-user"></i>Profile
                </a>
            </li>
            <li>
                <a class="dropdown-item dropdown-item-icon" href="javascript:;">
                    <i class="fa-key"></i>Password
                </a>
            </li>
            <li>
                <hr class="dropdown-divider" size="0">
            </li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a class="dropdown-item dropdown-item-icon" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                    this.closest('form').submit();">
                        <i class="fa-sign-out"></i>Logout
                    </a>
                </form>
            </li>
        </ul>
    </div>
    <div class="setting-menu dropdown"> <!--- xxx toggle dibawah untuk menghilangkan -->
        <a href="#" class="dropdown-togglexxx" id="dropdownSetting" data-bs-toggle="dropdown"
            data-bs-auto-close="outside" aria-expanded="false"><i class="fa-ellipsis-v"></i></a>
        <div class="dropdown-menu dropdown-menu-end dropdown-setting p-3" aria-labelledby="dropdownSetting">
            <div class="fw-bold mb-2">Theme</div>
            <div class="d-flex">
                <div class="w-50 mb-1 me-1">
                    <a href="javascript:;" class="display-setting display-theme" data-val="light">
                        <i class="fa-sun"></i>
                        Light
                    </a>
                </div>
                <div class="w-50 mb-1 ms-1">
                    <a href="javascript:;" class="display-setting display-theme" data-val="dark">
                        <i class="fa-moon"></i>
                        Dark
                    </a>
                </div>
            </div>
            <div class="d-flex mb-3">
                <div class="mt-1 w-100">
                    <a href="javascript:;" class="display-setting display-theme" data-val="dark2">
                        <i class="fa-moon-stars"></i>
                        Dark (Alternative)
                    </a>
                </div>
            </div>
            <div class="fw-bold mb-2">Desaign</div>
            <div class="d-flex mb-3">
                <div class="w-50 me-1">
                    <a href="javascript:;" class="display-setting display-design" data-val="flat">
                        <i class="fa-square"></i>
                        Flat
                    </a>
                </div>
                <div class="w-50 ms-1">
                    <a href="javascript:;" class="display-setting display-design active" data-val="shadow">
                        <i class="fa-clone"></i>
                        Shadow
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
