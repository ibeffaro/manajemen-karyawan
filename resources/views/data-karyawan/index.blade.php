<x-master-layout>
    <div class="panel-content p-3">
        <div class="card">
            <div class="card-header fw-bold">Data Karyawan</div>
            <div class="card-body">
                @can('create data-karyawan')
                    <div class="appinityTable-action-button text-start">
                        <div class="btn-group" role="group">
                            <a href="{{ route('data-karyawan.create') }}" class="btn btn-app btn-add" app-link="default"><i
                                    class="fa-plus"></i><span>Add Data Karyawan</span></a>
                        </div>
                    </div>
                @endcan

                <div id="datatable" data-table="datakaryawan-table" class="table-responsive">
                    {!! $dataTable->table(['class' => 'table table-stripped']) !!}
                </div>
            </div>
        </div>
        <div aria-hidden="true" tabindex="-1" id="modal-add" class="modal fade"></div>
    </div>
    @push('js')
        {!! $dataTable->scripts() !!}
    @endpush
</x-master-layout>
