<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title">Data Karyawan
                <small class="subtitle-info">Form Data</small>
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{ $action ?? null }}" method="POST" id="form-action" autocomplete="off">
                @csrf
                @if ($data->id)
                    @method('PUT')
                @endif
                <div class="mb-3 row">
                    <label for="name" class="col-md-3 required form-label">Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="name" name="name" aria-label="Name"
                            value="{{ $data->name }}">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="name" class="col-md-3 required form-label">Phone</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="phone" name="phone" aria-label="Phone"
                            value="{{ $data->phone }}">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="email" class="col-md-3 required form-label">Email</label>
                    <div class="col-md-9">
                        <div class="input-group iconpicker-container">
                            <input type="email" class="form-control" id="email" name="email" aria-label="Email"
                                value="{{ $data->email }}">
                        </div>
                    </div>
                </div>
                {{-- @if (request()->routeIs('data-karyawan.create')) --}}
                <div class="mb-3 row">
                    <label for="password" class="col-md-3 required form-label">Password</label>
                    <div class="col-md-9">
                        <div class="input-group iconpicker-container">
                            <input type="password" class="form-control" id="password" name="password"
                                aria-label="Password" value="">
                        </div>
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="password_confirmation" class="col-md-3 form-label">Confirm Password</label>
                    <div class="col-md-9">
                        <div class="input-group iconpicker-container">
                            <input type="password" class="form-control" id="password_confirmation"
                                name="password_confirmation" aria-label="Confirm Password" value="">
                        </div>
                    </div>
                </div>
                {{-- @endif --}}

                <div class="modal-footer">
                    @if ($action)
                        <button type="button" class="btn btn-theme" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-app">Save</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
