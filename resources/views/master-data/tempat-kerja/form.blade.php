<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title">Tempat Kerja
                <small class="subtitle-info">Form Data</small>
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{ $action ?? null }}" method="POST" id="form-action" autocomplete="off">
                @csrf
                @if ($data->id)
                    @method('PUT')
                @endif
                <div class="mb-3 row">
                    <label for="name" class="col-md-3 required form-label">Tempat Kerja</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="tempat_kerja" name="tempat_kerja"
                            aria-label="Tempat Kerja" value="{{ $data->tempat_kerja }}">
                    </div>
                </div>

                <div class="modal-footer">
                    @if ($action)
                        <button type="button" class="btn btn-theme" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-app">Save</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
